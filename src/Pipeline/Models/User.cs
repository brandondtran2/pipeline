﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pipeline.Models
{

    // TODO: This is just a test to see if Entity Framework is working!
    // TODO: Replace this with a proper User model later!!!
    public class User
    {
        public Guid ID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}
