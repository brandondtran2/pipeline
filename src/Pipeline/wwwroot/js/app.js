﻿(function() {
    var app = angular.module('pipeline', ['ngRoute', 'mobile-angular-ui']);

    app.config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/views/templates/home.html'
            })
            .when('/home', {
                templateUrl: '/views/templates/home.html'
            })
            .when('/about', {
                templateUrl: '/views/templates/about.html'
            })
            .when('/login', {
                templateUrl: '/views/templates/login.html'
            })
            .when('/dashboard', {
                templateUrl: '/views/templates/dashboard.html'
            })
            .when('/users', {
                templateUrl: '/views/templates/users.html',
                controller: 'UserController',
                controllerAs: 'userCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);
})();
