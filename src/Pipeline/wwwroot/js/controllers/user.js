﻿// TODO: THIS IS JUST A TEST FILE TO MAKE AN HTTP POST REQUEST TO THE USER CONTROLLER
// TODO: REMOVE OR REDO THIS FILE IN LATER

angular.module('pipeline').controller('UserController', [
    '$scope', '$http', function($scope, $http) {
        
        $scope.user = {};
        $scope.createUser = function() {
            $scope.user.ID = generateUUID();

            var data = {
                ID: $scope.user.ID,
                FirstName: $scope.user.FirstName,
                LastName: $scope.user.LastName,
                Email: $scope.user.Email
            }
            $http.post('api/users', JSON.stringify(data), { headers: {'Content-Type': 'application/json'}}).
                then(function successCallback(response) {
                    // this callback will be called asynchronously
                    // when the response is available
                    console.log(response);
                }, function errorCallback(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    console.log(response);
                });

            $scope.user = {};
        }

        function generateUUID() {
            var d = new Date().getTime();
            if (window.performance && typeof window.performance.now === "function") {
                d += performance.now(); //use high-precision timer if available
            }
            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
            return uuid;
        }
    }
]);