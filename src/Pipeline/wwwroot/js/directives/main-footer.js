﻿angular.module('pipeline').directive('mainFooter', function () {
    return {
        restrict: 'E',
        templateUrl: '/views/shared/footer.html'
    };
});